package simplelight.simplex.com.boxes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        print("Main")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun noSignin(view: View) {

    }

    fun googleSignin(view: View) {

    }

    fun canvasSignin(view: View) {

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.base, menu)
        return true
    }

}
